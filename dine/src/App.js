import "./App.css";
import { GetAPI } from "./components/GetAPI";
import { Main } from "./components/Main/Main";
import bootstrap from "bootstrap"

function App() {
  return (
    <div className="App">
      <GetAPI>
        <Main></Main>
      </GetAPI>
    </div>
  );
}

export default App;
