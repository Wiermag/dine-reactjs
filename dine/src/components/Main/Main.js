import React, { useState, useContext } from "react";
import { DineContext } from "../GetAPI";

import { Container} from "react-bootstrap";
import "./Main.css";

const Restaurant = () => {
  const [dataAllRestaurants] = useContext(DineContext);
  const [dataRestaurantDetails, setDataRestaurantDetails] = useState({});
  const [window, setWindow] = useState(false);

  const restaurant = dataAllRestaurants.map((item) => {
    const logDetails = async () => {
      const response = await fetch(`/restaurant/find/${item.id}`);
      const data = await response.json();
      setDataRestaurantDetails(data);
      setWindow(!window);
    };
    
    return (
      <div className="media" key={item.id}>
        <div className="media-body">
          <h5 className="mt-0">{item.restaurantName}</h5>
          <button type="button" className="btn btn-light" onClick={logDetails}>
            {!window ? "open" : "close"}
          </button>
          {window ? <RestaurantDetails item={item} /> : null}
        </div>
      </div>
    );
  });
  return (
    <div className="jumbotron">
      <h1 className="display-4">Dine</h1>
      <p className="lead">About app...</p>
      <hr className="my-4"></hr>
      <div>{restaurant}</div>
    </div>
  );
};
const RestaurantDetails = ({ item }) => {
  return (
    <div>
      {item.restaurantAddress}
      <br></br>
      {item.restaurantEmail}
      <br></br>
      {item.restaurantPhone}
    </div>
  );
};

const Main = () => {
  return (
    <Container className={`container-box`}>
      <Restaurant />
    </Container>
  );
};
export { Main };
