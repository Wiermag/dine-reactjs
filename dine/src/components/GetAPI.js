import React, { useEffect, useState, createContext } from "react";

const DineContext = createContext();

const GetAPI = (props) => {
  const [dataAllRestaurants, setDataAllRestaurants] = useState([]);

  useEffect(() => {
    getAllRestaurantsAPI();
  }, []);

  const getAllRestaurantsAPI = async () => {
    const response = await fetch(`/restaurant/find/all`);
    const data = await response.json();

    setDataAllRestaurants(data);
    console.log(data);
  };

  return (
    <DineContext.Provider value={[dataAllRestaurants, setDataAllRestaurants]}>
      {props.children}
    </DineContext.Provider>
  );
};

export { GetAPI, DineContext };
